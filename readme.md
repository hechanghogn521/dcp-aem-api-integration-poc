## Swagger generation

+ access to below swagger-ui for API specification: 
`http://localhost:8080/swagger-ui/index.html`

+ Download swagger.json via: 
`http://localhost8080/v2/api-docs`

+ Access to API endpoint: 
http://localhost:8080/api/students/10002

+ Open Ngrok
`ngrok http 8080`

+ copy the domain  an access to below endpoint: 
https://77bf92634d7b.ngrok.io/api/students/10002


